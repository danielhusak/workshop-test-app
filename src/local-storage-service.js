let STORAGE_KEY = 'hunt-game';

const DELAY = 1 * 100;

(function () {
  const orig = localStorage.setItem;
  localStorage.setItem = (key, item) => {
    return new Promise(resolve => {
      setTimeout(() => {
        orig.call(localStorage, key, item);
        resolve();
      }, DELAY);
    });
  };
})();

(function () {
  const orig = localStorage.getItem;
  localStorage.getItem = (key) => {
    return new Promise(resolve => {
      setTimeout(() => {
        const result = orig.call(localStorage, key);
        resolve(result);
      }, DELAY);
    });
  };
})();

export function saveScore(name, score) {
  return new Promise(resolve => {
    getScorers().then((scorers) => {
      scorers[name] = Math.max(score, scorers[name] || score);
      const promise = localStorage.setItem(STORAGE_KEY, JSON.stringify(scorers));
      promise.then(() => resolve());
    });

  });
}

export function getScorers() {
  return new Promise(resolve => {
    localStorage.getItem(STORAGE_KEY).then(data => {
      resolve(data !== null ? JSON.parse(data) : {});
    });
  });
}

export function getTopScorers() {
  return new Promise(resolve => {
    getScorers().then(scorers => {
      const topScorers = Object.keys(scorers)
              .map(name => ({name, score: scorers[name]}))
              .sort((a, b) => b.score - a.score)
              .slice(0, 5);
      resolve(topScorers);
    });
  });
}